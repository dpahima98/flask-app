from flask import Flask
from flask import render_template
import logging
import json
import os
import function as fun

app = Flask(__name__)
host, port, debug = "0.0.0.0", "8080", True

if os.path.exists("app.log"):
    logging.basicConfig(filename="app.log", filemode="a", level=logging.DEBUG)
else:
    logging.basicConfig(filename="app.log", filemode="w", level=logging.DEBUG)

if os.path.exists("db"):
    pass
else:
    os.mkdir("db")

user_dict = {"user1":{"name":"Daniel", "lname":"Pahima", "age":23}, "user2":{"name":"Alex", "lname":"Schapelle", "age":36}, 
            "user3":{"name":"Israel", "lname":"Israeli", "age":40}, "user4":{"name":"Dani", "lname":"Hen", "age":32},
            "user5":{"name":"Nofar", "lname":"Golan", "age":44}}

@app.route("/")
def index():
    try:
        logging.info("app is up")
        return render_template("index.html")
    except:
        logging.error("app cant upload ")

@app.route("/user_list")
def user_list():
    logging.info("print user list")
    return json.dumps(user_dict, indent= 4)

@app.route("/create_db/<dbname>")
def create_db(dbname):
    pathDB = f"db/{dbname}.db"
    if fun.checkFileDB(pathDB):
        fun.connectDB(pathDB)
    else:
        open(pathDB, "w")
        logging.info("DB file created")
        fun.connectDB(pathDB)
    return f"sucsess to connect DB file named {dbname}"

@app.route("/create_db/<dbname>/create_table")
def create_table(dbname):
    pathDB = f"db/{dbname}.db"
    if fun.checkFileDB(pathDB):
        logging.info("create user table")
        con = fun.connectDB(pathDB)  
        fun.createUserTable(con)
    else:
        logging.warning("DB file does not exist")
        create_db(dbname)
        con = fun.connectDB(pathDB)
        fun.createUserTable(con)

    return "sucsess create user table"


if __name__ == "__main__":
    app.run(host=host, port=port, debug=debug)