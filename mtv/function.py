import sqlite3
import logging
import os

def checkFileDB(pathDB):
    if os.path.exists(pathDB):
        logging.info("DB file is exist connect to DB")
        return True
    else:
        logging.warning("DB file doesnt exist")
        return False


def connectDB(dbname):
    con = None
    try:
        con = sqlite3.connect(dbname)
        logging.info("DB connect")
        return con
    except sqlite3.Error as e:
        logging.error(e)
        exit(1)
    con.close()

def createUserTable(con):
    cursor = con.cursor()
    try:
        cursor.execute(
                    """
        CREATE TABLE USERS (
        ID     INTEGER PRIMARY KEY AUTOINCREMENT,
        NAME   TEXT NOT NULL
        );
        """
        )
        logging.info("create user table")
    except sqlite3.Error as e:
        logging.error(e)
        exit(1)
    cursor.close()
    con.close()
    